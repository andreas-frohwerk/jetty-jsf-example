package de.frohwerk.jsf;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.util.resource.Resource;
import org.eclipse.jetty.webapp.WebAppContext;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FacesExampleApplication {
    public static void main(String[] args) throws Exception {
        // Configure logging to use log4j2 framework
        System.setProperty("java.util.logging.config.class", "org.apache.logging.log4j.jul.LogManager");
        org.eclipse.jetty.util.log.Log.setLog(new org.eclipse.jetty.util.log.Slf4jLog());

        // Configure jetty configuration classes
        final List<String> configurationClasses = new ArrayList<>();
        Collections.addAll(configurationClasses, WebAppContext.getDefaultConfigurationClasses());
        configurationClasses.add(org.eclipse.jetty.plus.webapp.PlusConfiguration.class.getName());
        configurationClasses.add(org.eclipse.jetty.plus.webapp.EnvConfiguration.class.getName());
        configurationClasses.add(org.eclipse.jetty.annotations.AnnotationConfiguration.class.getName());

        // Configure web application static files and classpath
        final WebAppContext webAppContext = new WebAppContext();
        webAppContext.setConfigurationClasses(configurationClasses);
        webAppContext.setBaseResource(Resource.newResource("src/main/resources"));
        webAppContext.getMetaData().addWebInfJar(Resource.newResource("src/main/resources"));
        webAppContext.getMetaData().getWebInfClassesDirs().add(Resource.newClassPathResource("/"));

        // Create server
        final Server server = new Server(8090);
        server.setHandler(webAppContext);

        // Start server
        server.start();
        server.join();
    }
}
