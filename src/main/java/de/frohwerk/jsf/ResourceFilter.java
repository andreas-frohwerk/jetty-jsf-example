package de.frohwerk.jsf;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter("*")
public class ResourceFilter implements Filter {
    @Override
    public void init(final FilterConfig filterConfig) { /* Do nothing */ }

    @Override
    public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain) throws ServletException, IOException {
        if (request instanceof HttpServletRequest && response instanceof HttpServletResponse) {
            final HttpServletRequest httpServletRequest = (HttpServletRequest) request;

            if (httpServletRequest.getRequestURI().endsWith(".xhtml")) {
                chain.doFilter(request, response);
            } else {
                final HttpServletResponse httpServletResponse = (HttpServletResponse) response;
                httpServletResponse.setStatus(HttpServletResponse.SC_FORBIDDEN);
                httpServletResponse.setContentType("text/plain");
                httpServletResponse.getWriter().println("Forbidden");
            }
        } else {
            throw new ServletException("Invalid response type");
        }
    }

    @Override
    public void destroy() { /* Do nothing */ }
}
