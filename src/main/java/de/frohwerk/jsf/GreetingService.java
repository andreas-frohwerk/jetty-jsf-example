package de.frohwerk.jsf;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class GreetingService {
    private static final Logger logger = LogManager.getLogger(GreetingService.class);

    public String createGreeting(final String name) {
        logger.trace("GreetingService::createGreeting({})", name);
        return "Hello " + name + "!";
    }
}
