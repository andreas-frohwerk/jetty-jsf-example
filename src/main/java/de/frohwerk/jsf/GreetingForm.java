package de.frohwerk.jsf;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

@Named
@RequestScoped
public class GreetingForm implements Serializable {
    private static final Logger logger = LogManager.getLogger(GreetingForm.class);

    private transient final GreetingService greetingService;

    private String name = "";
    private String message = "";

    @Inject
    public GreetingForm(final GreetingService greetingService) {
        logger.trace("GreetingForm::new | {}", name);
        this.greetingService = greetingService;
    }

    public String getName() {
        logger.trace("GreetingForm::getName() | {}", name);
        return name;
    }

    public void setName(String name) {
        logger.trace("GreetingForm::setName({})", name);
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public void submit() {
        logger.trace("GreetingForm::submit | {}", name);
        message = name == null || name.equals("") ? null : greetingService.createGreeting(name);
    }

    @Override
    public String toString() {
        return "GreetingForm" +
                       "{ name='" + name + '\'' +
                       ", message='" + message + '\'' +
                       '}';
    }
}
